import pygame
import time
import random
import math


def funk(a):
    return 1 if a else -1


def rot_to_point(screen, image, point_pos, pos, suze=1, angles=0, flip_v=False, flip_g=False):
    # player_pos = screen.get_rect().center
    # print(image, point_pos, pos)
    player_pos = pos
    player_rect = image.get_rect(center=player_pos)

    mx, my = point_pos
    dx, dy = mx - player_rect.centerx, my - player_rect.centery

    angle = math.degrees(math.atan2(-dy, dx))

    rot_image = pygame.transform.flip(image, flip_g, flip_v)

    rot_image = pygame.transform.rotate(rot_image, angle - angles)

    rot_image = pygame.transform.scale(rot_image, (rot_image.get_width() * suze, rot_image.get_height() * suze))

    rot_image_rect = rot_image.get_rect(center=player_rect.center)

    screen.blit(rot_image, rot_image_rect.topleft)


def text_bilder(text, cord, size=20.0,  text_allowed_simvols='1234567890absdefghijkrtyxzsqcpoltvmnw_!u-',
                screen=pygame.display.set_mode((200, 200))):
    now_cord = cord.copy()
    # screen.fill(0)
    for i in text:
        if i in text_allowed_simvols:
            screen.blit(pygame.transform.scale(pygame.image.load(f'images/{i}.png'), (size, size)), now_cord)
            now_cord[0] += size + 1
            # pygame.display.flip()
        elif i == ':':
            screen.blit(pygame.transform.scale(pygame.image.load('images/points.png'), (size, size)), now_cord)
            now_cord[1] += size + 1


def draw_button(screen=pygame.display.set_mode((1920, 1080)), size=(100, 100), text='', cord=(0, 0), text_size=20):
    screen_balance = (screen.get_size()[0] / 1920, screen.get_size()[1] / 1080)
    if abs(pygame.mouse.get_pos()[0] - cord[0] - size[0] // 3) <= size[0] // 2 * screen_balance[0] and\
            abs(cord[1] + size[1] // 3 - pygame.mouse.get_pos()[1]) <= size[1] // 2 * screen_balance[1]:
        play_button = pygame.image.load('images/button_on.png')
    else:
        play_button = pygame.image.load('images/button.png')
    screen.blit(pygame.transform.scale(play_button,
                                       (size[0] * screen_balance[0],
                                        size[1] * screen_balance[1])), (cord[0] - size[0] // 3, cord[1] - size[1] // 3))
    # pygame.display.flip()
    text_bilder(text, [cord[0] - size[0] // 5, cord[1] - size[1] // 5], size=text_size)
    return abs(pygame.mouse.get_pos()[0] - cord[0] - size[0] // 3) <= size[0] // 2 * screen_balance[0]\
        and abs(cord[1] + size[1] // 3 - pygame.mouse.get_pos()[1]) <= size[1] // 2 * screen_balance[1]
    # pygame.display.flip()


def target_ai(target, you, watching_distance):
    if abs(target[0] - you[0]) <= watching_distance and abs(target[1] - you[1]) <= watching_distance:
        return -1 if target[0] > you[0] else 1, -1 if target[1] > you[1] else 1
    else:
        return False


def game(screen=pygame.display.set_mode((1920, 1080), pygame.FULLSCREEN), max_items=150
         , groun_color=(70 / 30, 100 / 30, 0)):
    # print(int(False))
    camera = [0, 0]
    # print((100)[0])
    # {'type': 'rabbit', 'jamp_vector': (0, 0, 0), 'jump_impulse': 0, 'cord': (100, 100)}
    rabbits = [{'type': 'rabbit', 'vector': [-1, -1], 'pos': [100, 100], 'animation_frame': 0, 'max_speed': 15,
                'fox': random.choice((False, False, False, False, True, False, False)),
                'hanger': 100, 'kids': [], 'dont_want_to_reproduction_bar': 100, 'poop_progress': 0,
                        'infected': False, 'grow': 100}]
    for n in range(random.randint(50, max_items)):
        rabbits.append({'type': 'grass', 'pos': (random.randint(camera[0] - 2000 + screen.get_height() // 2,
                                                                camera[0] + 2000 - screen.get_height() // 2),
                                                 random.randint(camera[1] - 2000 + screen.get_width() // 2,
                                                                camera[1] + 2000 - screen.get_width() // 2)),
                        'animation_frame': random.randint(0, 3), 'flipped': random.randint(0, 1)})
    siza = 2
    moahos = (0, 0)
    lustmoahos = moahos
    see_mod = False
    keynow = []
    camera_move = [0, 0]
    for v in range(1, random.randint(10, 200)):
        rabbits.append({'type': 'rabbit', 'vector': [-1, -1],
                        'pos': [random.randint(-1000, 1000), random.randint(-1000, 1000)],
                        'animation_frame': 0, 'max_speed': random.randint(10, 20),
                        'fox': random.choice((False, False, False, False, True, False, False)),
                        'hanger': 100, 'kids': [], 'dont_want_to_reproduction_bar': random.randint(0, 100),
                        'poop_progress': 0,
                        'infected': False, 'grow': 100})

    for _ in range(random.randint(2, 5)):
        vj = (random.randint(-10000, 10000), random.randint(-10000, 10000))
        for _ in range(random.randint(6, 11)):
            rabbits.append({'type': 'pinaua_fruit',
                            'pos': (random.randint(int(vj[0] - 1050),
                                                   int(vj[0] + 1050)),
                                    random.randint(vj[1] - 1050,
                                                   vj[1] + 1050)),
                            'food': 100, 'age': 0, 'food_grow': 100,
                            'infected': False}) # 'infected': random.choice((False, False, False, False, True))
    chingu = False
    cinematic_camera = False
    cinmet = False
    show_parameters = False
    show_food = False
    sap = False
    next_cwst_spown = 100
    rabbits.append({'type': 'food', 'pos': (960,
                                            540),
                    'food': 100, 'age': 0})
    # rabbits.append({'type': 'p_plant', 'grow': 0,
    #                 'pos': (980,
    #                         590)})
    # print(0.0066666666666667 * 15, 0.0066666666666667 * 20)
    fps = pygame.time.Clock()
    count_grass = 0
    arrow_food = False
    show_pinaua = False
    while True:
        count_rabbits = 0
        count_food = 0
        count_pinaua = 0
        count_fox = 0
        # cinematic_camera = False
        # print(pygame.mouse.get_pressed()[1], moahos, pygame.mouse.get_pos())
        if (lustmoahos != pygame.mouse.get_pos() and pygame.mouse.get_pressed()[1]) or chingu:
            camera[0] += lustmoahos[0] - pygame.mouse.get_pos()[0]
            camera[1] += lustmoahos[1] - pygame.mouse.get_pos()[1]
            # if cinematic_camera:
            #     camera_move[0] += (lustmoahos[0] - pygame.mouse.get_pos()[0]) / 3
            #     camera_move[1] += (lustmoahos[1] - pygame.mouse.get_pos()[1]) / 3
            # count_grass = 0
            # for b in rabbits:
            #     if b['type'] == 'grass':
            #         count_grass += 1
            for n in range(random.randint(50, max_items) - count_grass):
                rabbits.append({'type': 'grass', 'pos': (random.randint(camera[0] - 2000 + screen.get_height() // 2,
                                                                        camera[0] + 2000 - screen.get_height() // 2),
                                                         random.randint(camera[1] - 2000 + screen.get_width() // 2,
                                                                        camera[1] + 2000 - screen.get_width() // 2)),
                                'animation_frame': random.randint(0, 3), 'flipped': random.randint(0, 1)})
                pass
            # print('abobus')
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                # print(event)
                if event.key == 61:
                    siza += 0.1
                if event.key == 45:
                    siza -= 0.1
                if event.key == 8:
                    siza = 2
                if event.key == 118:
                    if see_mod:
                        see_mod = False
                    else:
                        see_mod = True
                if 99 == event.key:
                    if show_parameters:
                        show_parameters = False
                    else:
                        show_parameters = True
                if 120 == event.key:

                    if show_food:
                        show_food = False
                    else:
                        show_food = True

                    if sap:
                        sap = False
                if 103 == event.key:

                    if sap:
                        sap = False
                    else:
                        sap = True
                    if show_food:
                        show_food = False
                if 106 == event.key:
                    if arrow_food:
                        arrow_food = False
                    else:
                        arrow_food = True
                if 112 == event.key:
                    if show_pinaua:
                        show_pinaua = False
                    else:
                        show_pinaua = True
                keynow.append(event.key)
            if event.type == pygame.KEYUP:
                if event.key in keynow:
                    keynow.remove(event.key)
        chingu = False
        voi = 10
        if 1073742048 in keynow and not cinmet:
            if cinematic_camera:
                cinematic_camera = False
            else:
                cinematic_camera = True
            cinmet = True
            # print('bibobus')
        elif 1073742048 not in keynow:
            cinmet = False

        if 1073742049 in keynow:
            voi += 30
        if 97 in keynow:
            if cinematic_camera:
                camera_move[0] = -voi - 10
            else:
                camera[0] -= voi + 10
            chingu = True
        if 115 in keynow:
            if cinematic_camera:
                camera_move[1] = voi + 10
            else:
                camera[1] += voi
            chingu = True
        if 100 in keynow:
            if cinematic_camera:
                camera_move[0] = voi + 10
            else:
                camera[0] += voi
            chingu = True
        if 119 in keynow:
            if cinematic_camera:
                camera_move[1] = -voi - 10
            else:
                camera[1] -= voi
            chingu = True
        camera[0] += camera_move[0]
        camera[1] += camera_move[1]
        # print(camera_move, cinematic_camera)
        if cinematic_camera:
            if camera_move[0] < 0:
                camera_move[0] += 1
            if camera_move[0] > 0:
                camera_move[0] -= 1

            if camera_move[1] < 0:
                camera_move[1] += 1
            if camera_move[1] > 0:
                camera_move[1] -= 1
            chingu = True
        else:
            camera_move[0] = 0
            camera_move[1] = 0
        # print(camera_move)
        custil = 0

        count_grass = 0
        for indrabit, rabbit in enumerate(rabbits[:]):
            if rabbit['type'] == 'rabbit':
                if rabbit['hanger'] <= 0:
                    del rabbits[indrabit - custil]
                    custil += 1
                else:
                    if not rabbit['fox']:
                        count_rabbits += 1
                    else:
                        count_fox += 1
                    if (abs(rabbit['vector'][0]) or abs(rabbit['vector'][1])) and \
                            abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() + 100 and \
                            abs(rabbit['pos'][1] - camera[1] - screen.get_height() // 2) <= screen.get_height() + 100:
                        if 1.5 > rabbit['animation_frame'] >= 0:
                            # print(rabbit['animation_frame'], 'j')
                            if rabbit['fox']:
                                mimig = pygame.image.load('images/fix.png')
                            else:
                                mimig = pygame.image.load('images/rubbit.png')
                            rabbits[indrabit - custil]['animation_frame'] += 0.1
                        elif rabbit['animation_frame'] >= 1.5:
                            if rabbit['fox']:
                                mimig = pygame.image.load('images/fix_walk.png')
                            else:
                                mimig = pygame.image.load('images/rubbit2.png')
                            if rabbit['animation_frame'] < 2.5:
                                # print(rabbit['animation_frame'])
                                rabbits[indrabit - custil]['animation_frame'] += 0.1
                            else:
                                # print(rabbit['animation_frame'], 'd')
                                rabbits[indrabit - custil]['animation_frame'] = 0
                    else:
                        if rabbit['fox']:
                            mimig = pygame.image.load('images/fix.png')
                        else:
                            mimig = pygame.image.load('images/rubbit.png')
                        rabbits[indrabit - custil]['animation_frame'] = 0
                    if random.randint(1, 100) <= 5:
                        rabbits[indrabit - custil]['vector'][0] += random.randint(-1, 1)
                        rabbits[indrabit - custil]['vector'][1] += random.randint(-1, 1)

                    last_food = float('inf')

                    for t, y in enumerate(rabbits):
                        if y['type'] != 'grass':

                            if not bool(rabbit['fox']) and\
                                    (y['type'] == 'food' or y['type'] == 'pinaua_fruit') and int(y['food'])\
                                    and abs(y['pos'][0] - rabbit['pos'][0])\
                                    + abs(y['pos'][1] - rabbit['pos'][1]) <= last_food:
                                if rabbit['hanger'] <= 70:
                                    g = target_ai(y['pos'], rabbit['pos'], 1000)
                                    if g:
                                        rabbits[indrabit - custil]['vector'][0] += g[0]
                                        rabbits[indrabit - custil]['vector'][1] += g[1]
                                        last_food = abs(y['pos'][0] - rabbit['pos'][0]) + abs(y['pos'][1] - rabbit['pos'][1])
                                if rabbit['hanger'] < 100 and abs(y['pos'][0] - rabbit['pos'][0])\
                                        + abs(y['pos'][1] - rabbit['pos'][1]) <= 100:
                                    rabbits[t]['food'] -= 1
                                    if y['type'] == 'pinaua_fruit':
                                        if rabbit['infected'] and random.randint(1, 100) <= 3:
                                            rabbits[t]['infected'] = True
                                        # print(rabbits[indrabit])
                                        if y['infected'] and random.randint(1, 100) <= 10:
                                            rabbits[indrabit - custil]['infected'] = True
                                    rabbits[indrabit - custil]['hanger'] += 1
                                    rabbits[indrabit - custil]['vector'][1] //= 3
                                    rabbits[indrabit - custil]['vector'][0] //= 3
                                    if random.randint(1, 100) <= 20:
                                        rabbits[indrabit - custil]['poop_progress'] += 1

                            elif y['type'] == 'rabbit' and not bool(y['fox']) and bool(rabbit['fox']):
                                duk = abs(y['pos'][0] - rabbit['pos'][0]) + abs(y['pos'][1] - rabbit['pos'][1])
                                if rabbit['hanger'] <= 70 or (duk < last_food and duk >= 800):
                                    g = target_ai(y['pos'], rabbit['pos'], 1000)
                                    if g:
                                        if rabbit['hanger'] <= 70 and duk >= 800:
                                            rabbits[indrabit - custil]['vector'][0] += g[0] * 2
                                            rabbits[indrabit - custil]['vector'][1] += g[1] * 2
                                        elif rabbit['hanger'] > 70 and duk <= 800:
                                            rabbits[indrabit - custil]['vector'][0] -= g[0]
                                            rabbits[indrabit - custil]['vector'][1] -= g[1]
                                        last_food = abs(y['pos'][0] - rabbit['pos'][0]) + abs(
                                            y['pos'][1] - rabbit['pos'][1])
                                if rabbit['hanger'] < 80 and abs(y['pos'][0] - rabbit['pos'][0]) \
                                        + abs(y['pos'][1] - rabbit['pos'][1]) <= 100:
                                    rabbits[indrabit - custil]['hanger'] += y['hanger'] / 100 * y['grow']

                                    rabbits[t]['hanger'] -= y['hanger']

                                    rabbits[t]['vector'][1] //= 3
                                    rabbits[t]['vector'][0] //= 3
                                    rabbits[indrabit - custil]['vector'][1] //= 3
                                    rabbits[indrabit - custil]['vector'][0] //= 3

                            if not rabbit['fox'] and y['type'] == 'rabbit' and y['fox']:
                                g = target_ai(y['pos'], rabbit['pos'], 500)
                                if g:
                                    rabbits[indrabit - custil]['vector'][0] -= g[0]
                                    rabbits[indrabit - custil]['vector'][1] -= g[1]
                                    last_food = abs(y['pos'][0] - rabbit['pos'][0]) + abs(
                                        y['pos'][1] - rabbit['pos'][1])

                            if y != rabbit and rabbit['dont_want_to_reproduction_bar'] < 40 and y['type'] == 'rabbit' and len(y['kids']) <= 3\
                                    and abs(y['pos'][0] - rabbit['pos'][0])\
                                    + abs(y['pos'][1] - rabbit['pos'][1]) <= last_food - rabbit['dont_want_to_reproduction_bar']:
                                g = target_ai(y['pos'], rabbit['pos'], 1000)
                                if g:
                                    rabbits[indrabit - custil]['vector'][0] += g[0]
                                    rabbits[indrabit - custil]['vector'][1] += g[1]
                                    last_food = abs(y['pos'][0] - rabbit['pos'][0]) + abs(
                                        y['pos'][1] - rabbit['pos'][1])

                                if rabbit['dont_want_to_reproduction_bar'] < 70 and abs(y['pos'][0] - rabbit['pos'][0])\
                                        + abs(y['pos'][1] - rabbit['pos'][1]) <= 100:
                                    # rabbits[t]['food'] -= 1
                                    # print(rabbits[indrabit])
                                    # rabbits[indrabit - custil]['hanger'] += 1
                                    rabbits[t]['kids'].append([random.randint(min(y['max_speed'],
                                                                                  rabbit['max_speed']) + 1,
                                                                              max(y['max_speed'],
                                                                                  rabbit['max_speed']) + 1), 100])
                                    rabbits[indrabit - custil]['vector'][1] //= 3
                                    rabbits[indrabit - custil]['vector'][0] //= 3
                                    rabbits[indrabit - custil]['dont_want_to_reproduction_bar'] = random.randint(100, 500)
                        elif y != rabbit and y['type'] == 'rabbit' and rabbit['dont_want_to_reproduction_bar'] > 70:
                            g = target_ai(y['pos'], rabbit['pos'], 100)
                            if g:
                                rabbits[indrabit - custil]['vector'][0] -= g[0]
                                rabbits[indrabit - custil]['vector'][1] -= g[1]

                    if rabbit['kids']:
                        for bb, b in enumerate(rabbit['kids'][:]):
                            if b[-1] <= 0:
                                rabbits.append({'type': 'rabbit', 'vector': [-rabbit['vector'][0], -rabbit['vector'][1]],
                                                'pos': [rabbit['pos'][0] + random.randint(-10, 10),
                                                        rabbit['pos'][1] + random.randint(-10, 10)],
                                                'animation_frame': 0, 'max_speed': b[0],
                                                'fox': rabbit['fox'], 'hanger': 100, 'kids': [],
                                                'dont_want_to_reproduction_bar': random.randint(100, 1000), 'poop_progress': 0,
                                                'infected': False, 'grow': 0})
                                del rabbits[indrabit - custil]['kids'][bb]
                                break
                            else:
                                # print(rabbit)
                                rabbits[indrabit - custil]['kids'][bb][-1] -= 0.5
                        pass

                    if rabbit['poop_progress'] > 0:
                        rabbits[indrabit - custil]['poop_progress'] += 0.1
                    if rabbit['poop_progress'] >= 100:
                        rabbits.append({'type': 'p_plant', 'grow': -20,
                                        'pos': (rabbit['pos'][0] + random.randint(-100, 100),
                                                rabbit['pos'][1] + random.randint(-100, 100)),
                                        'infected': bool(int(rabbit['infected']) + random.randint(-1, 1))})
                        rabbits[indrabit - custil]['poop_progress'] = 0

                    if rabbit['dont_want_to_reproduction_bar'] > 0:
                        rabbits[indrabit - custil]['dont_want_to_reproduction_bar'] -= 0.1

                    if rabbit['max_speed'] + int(rabbit['fox']) * 10 < abs(rabbit['vector'][0]):
                        rabbits[indrabit - custil]['vector'][0] //= 2
                    if rabbit['max_speed'] + int(rabbit['fox']) * 10 < abs(rabbit['vector'][1]):
                        rabbits[indrabit - custil]['vector'][1] //= 2

                    elif random.uniform(0.01, 10) <= 0.035:
                        rabbits[indrabit - custil]['vector'][0] = 0
                        rabbits[indrabit - custil]['vector'][1] = 0

                    # print(rabbits[indrabit]['pos'])
                    rabbits[indrabit - custil]['pos'][0] -= rabbit['vector'][0]
                    rabbits[indrabit - custil]['pos'][1] -= rabbit['vector'][1]
                    # print(rabbit['vector'][0] + 1)
                    rot_to_point(screen, point_pos=(rabbit['pos'][0] - camera[0],
                                                    rabbit['pos'][1] - camera[1]),  # - rabbit['vector'][0]
                                 pos=(rabbit['pos'][0] - camera[0],
                                      rabbit['pos'][1] - camera[1]),
                                 flip_g=bool(rabbit['vector'][0] + 1 < 0),
                                 image=mimig, suze=siza / (int(rabbit['grow'] < 100) + 1), flip_v=False)
                    if rabbit['grow'] < 100:
                        rabbits[indrabit - custil]['grow'] += 0.1
                    if rabbit['infected'] and random.uniform(0.001, 10) <= 0.01:
                        rabbits[indrabit - custil]['infected'] = False
                    if show_food:
                        # print(f"{rabbit['hanger']}")
                        text_bilder(text=f"{int(rabbit['hanger'])}", cord=[rabbit['pos'][0] - camera[0],
                                                                           rabbit['pos'][1] - camera[1]], screen=screen)
                    if sap:
                        text_bilder(text=f"h{int(rabbit['hanger'])}_s{int(rabbit['max_speed'])}"
                                         f"_wl{int(rabbit['dont_want_to_reproduction_bar'])}",
                                    cord=[rabbit['pos'][0] - camera[0] - 50,
                                          rabbit['pos'][1] - camera[1] - 50], screen=screen)

                    if not next_cwst_spown + random.randint(-10, 10) and random.randint(1, 100) <= 60:
                        # print(rabbit['pos'], int(rabbit['pos'][1] + 1500))
                        rabbits.append({'type': 'food',
                                        'pos': (random.randint(int(rabbit['pos'][0] - 5050),
                                                               int(rabbit['pos'][0] + 5050)),
                                                random.randint(int(rabbit['pos'][1] - 5050),
                                                int(rabbit['pos'][1] + 5050))),
                                        'food': 100, 'age': 0})

                    rabbits[indrabit - custil]['hanger'] -= 0.0066666666666667 * rabbit['max_speed'] # 0.1
                    # print(rabbits[indrabit - custil]['hanger'])

                    if see_mod:
                        if abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() - 550 and \
                                abs(rabbit['pos'][1] - camera[1] - screen.get_height() // 2) <= screen.get_height() - 550:
                            pass
                        else:
                            vv = [screen.get_width() // 2, screen.get_height() // 2]
                            for _ in range(screen.get_width() // 2):
                                if rabbit['pos'][0] - camera[0] < vv[0]:
                                    vv[0] -= 1
                                if rabbit['pos'][0] - camera[0] > vv[0]:
                                    vv[0] += 1

                            for _ in range(screen.get_height() // 2):
                                if rabbit['pos'][1] - camera[1] < vv[1]:
                                    vv[1] -= 1
                                if rabbit['pos'][1] - camera[1] > vv[1]:
                                    vv[1] += 1

                            # print(vv, vv[1], 100, vv[1] <= 100)
                            # if abs(vv[0] - camera[0] - screen.get_width() // 2) >= screen.get_width():
                            if vv[0] <= 100:
                                vv[0] = 100
                            elif vv[0] >= screen.get_width():
                                vv[0] = screen.get_width() - 100

                            if vv[1] <= 100:
                                vv[1] = 100
                            elif vv[1] >= screen.get_height():
                                vv[1] = screen.get_height() - 100

                            # print(vv)

                            rot_to_point(screen, image=pygame.image.load('images/arrow_rabbit.png'),
                                         point_pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]), pos=vv,
                                         suze=siza)

                    pass
            if rabbit['type'] == 'grass':
                if abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() + 150 and \
                        abs(rabbit['pos'][1] - camera[1] - screen.get_height() // 2) <= screen.get_height() + 150:

                    if abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() + 100 and \
                            abs(rabbit['pos'][1] - camera[1] - screen.get_height() // 2) <= screen.get_height() + 100:

                        if rabbit['animation_frame'] == 0:
                            mimig = pygame.image.load('images/grass1.png')
                        elif rabbit['animation_frame'] == 1:
                            mimig = pygame.image.load('images/grass2.png')
                        elif rabbit['animation_frame'] == 2:
                            mimig = pygame.image.load('images/grass4.png')
                        elif rabbit['animation_frame'] >= 3:
                            mimig = pygame.image.load('images/grass3.png')

                        rot_to_point(screen, point_pos=(rabbit['pos'][0] - camera[0],
                                                        rabbit['pos'][1] - camera[1]),  # - rabbit['vector'][0]
                                     pos=(rabbit['pos'][0] - camera[0],
                                          rabbit['pos'][1] - camera[1]),
                                     image=mimig, suze=siza, flip_g=bool(rabbit['flipped']))
                        count_grass += 1
                else:
                    del rabbits[indrabit - custil]
                    custil += 1
            if rabbit['type'] == 'food':
                if rabbit['food'] <= 0 or rabbit['age'] >= 500:
                    del rabbits[indrabit - custil]
                    custil += 1
                else:
                    mimig = pygame.image.load('images/food.png')
                    rot_to_point(screen, point_pos=(rabbit['pos'][0] - camera[0],
                                                    rabbit['pos'][1] - camera[1]),  # - rabbit['vector'][0]
                                 pos=(rabbit['pos'][0] - camera[0],
                                      rabbit['pos'][1] - camera[1]),
                                 image=mimig, suze=siza)
                    count_food += 1
                    rabbits[indrabit - custil]['age'] += 1
                    if arrow_food:
                        if abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() - 550 and \
                                abs(rabbit['pos'][1] - camera[1]
                                    - screen.get_height() // 2) <= screen.get_height() - 550:
                            pass
                        else:
                            vv = [screen.get_width() // 2, screen.get_height() // 2]
                            for _ in range(screen.get_width() // 2):
                                if rabbit['pos'][0] - camera[0] < vv[0]:
                                    vv[0] -= 1
                                if rabbit['pos'][0] - camera[0] > vv[0]:
                                    vv[0] += 1

                            for _ in range(screen.get_height() // 2):
                                if rabbit['pos'][1] - camera[1] < vv[1]:
                                    vv[1] -= 1
                                if rabbit['pos'][1] - camera[1] > vv[1]:
                                    vv[1] += 1

                            # print(vv, vv[1], 100, vv[1] <= 100)
                            # if abs(vv[0] - camera[0] - screen.get_width() // 2) >= screen.get_width():
                            if vv[0] <= 100:
                                vv[0] = 100
                            elif vv[0] >= screen.get_width():
                                vv[0] = screen.get_width() - 100

                            if vv[1] <= 100:
                                vv[1] = 100
                            elif vv[1] >= screen.get_height():
                                vv[1] = screen.get_height() - 100

                            # print(vv)

                            rot_to_point(screen, image=pygame.image.load('images/arrow_food.png'),
                                         point_pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]), pos=vv,
                                         suze=siza)
            if rabbit['type'] == 'pinaua_fruit':
                if rabbit['food_grow'] > 100:
                    # print(int(rabbit['infected']))
                    rabbits[indrabit - custil]['food'] += 100 // (int(rabbit['infected']) + 1)
                    rabbits[indrabit - custil]['food_grow'] = 0

                if rabbit['infected']:
                    if int(rabbit['food']):
                        mimig = pygame.image.load('images/pinaua_tree_infected_fruits.png')
                    else:
                        rabbits[indrabit - custil]['food_grow'] += 1

                        mimig = pygame.image.load('images/pinaua_tree_infected.png')
                else:
                    if int(rabbit['food']):
                        mimig = pygame.image.load('images/pinaua_fruit_tree_fruits.png')
                    else:
                        rabbits[indrabit - custil]['food_grow'] += 1 / (int(rabbit['infected']) + 1)
                        mimig = pygame.image.load('images/pinaua_fruit_tree.png')
                rabbits[indrabit - custil]['age'] += 1
                if rabbit['age'] >= 1000 + random.randint(-10, 10):
                    tt = random.randint(-1, 3)
                    for _ in range(tt + 1 - int(rabbit['infected'])):
                        if random.randint(1, 100) <= 90:
                            infuct = rabbit['infected']
                        else:
                            infuct = not rabbit['infected']
                        rabbits.append({'type': 'p_plant', 'grow': 0,
                                        'pos': (rabbit['pos'][0] + random.randint(-200, 200),
                                                rabbit['pos'][1] + random.randint(-200, 200)),
                                        'infected': infuct})
                    del rabbits[indrabit - custil]
                    custil += 1

                rot_to_point(screen, image=mimig,
                             point_pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]),
                             pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]),
                             suze=siza)

                if show_pinaua:
                    if abs(rabbit['pos'][0] - camera[0] - screen.get_width() // 2) <= screen.get_width() - 550 and \
                            abs(rabbit['pos'][1] - camera[1]
                                - screen.get_height() // 2) <= screen.get_height() - 550:
                        pass
                    else:
                        vv = [screen.get_width() // 2, screen.get_height() // 2]
                        for _ in range(screen.get_width() // 2):
                            if rabbit['pos'][0] - camera[0] < vv[0]:
                                vv[0] -= 1
                            if rabbit['pos'][0] - camera[0] > vv[0]:
                                vv[0] += 1

                        for _ in range(screen.get_height() // 2):
                            if rabbit['pos'][1] - camera[1] < vv[1]:
                                vv[1] -= 1
                            if rabbit['pos'][1] - camera[1] > vv[1]:
                                vv[1] += 1

                        # print(vv, vv[1], 100, vv[1] <= 100)
                        # if abs(vv[0] - camera[0] - screen.get_width() // 2) >= screen.get_width():
                        if vv[0] <= 100:
                            vv[0] = 100
                        elif vv[0] >= screen.get_width():
                            vv[0] = screen.get_width() - 100

                        if vv[1] <= 100:
                            vv[1] = 100
                        elif vv[1] >= screen.get_height():
                            vv[1] = screen.get_height() - 100

                        # print(vv)

                        rot_to_point(screen, image=pygame.image.load('images/arrow_pinaua.png'),
                                     point_pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]), pos=vv,
                                     suze=siza)

                count_pinaua += 1
                if random.uniform(1, 10) <= rabbit['age'] / 900:
                    del rabbits[indrabit - custil]
                    custil += 1
            if rabbit['type'] == 'p_plant':
                if rabbit['infected'] and random.randint(1, 100) <= 70:
                    infuct = True
                else:
                    infuct = False
                if rabbit['grow'] >= 100 + random.randint(-10, 10):
                    rabbits.append({'type': 'pinaua_fruit',
                                    'pos': rabbit['pos'],
                                    'food': 0, 'age': 0, 'food_grow': 0,
                                    'infected': infuct})
                    del rabbits[indrabit - custil]
                    custil += 1
                else:
                    if rabbit['grow'] < 10:
                        mimig = pygame.image.load('images/pinauaseed.png')
                    else:
                        mimig = pygame.image.load('images/pinaua_tree_plant.png')
                    rabbits[indrabit - custil]['grow'] += 1
                    rot_to_point(screen, image=mimig,
                                 point_pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]),
                                 pos=(rabbit['pos'][0] - camera[0], rabbit['pos'][1] - camera[1]),
                                 suze=siza)
                    if rabbit['grow'] < 20 and random.randint(1, 100) <= 2:
                        del rabbits[indrabit - custil]
                        custil += 1
        if show_parameters:
            text_bilder(text=f'{len(rabbits)}_objects_now', cord=[100, 100], screen=screen)
            text_bilder(text=f'{count_rabbits}_rabbits_now', cord=[500, 100], screen=screen)
            text_bilder(text=f'{count_food}_food_now', cord=[100, 150], screen=screen)
            text_bilder(text=f'{count_pinaua}_pinauas_now', cord=[500, 150], screen=screen)
            text_bilder(text=f'{camera[0]}_{camera[1]}_camera_plase', cord=[100, 200], screen=screen)
            text_bilder(text=f'{count_fox}_foxes', cord=[100, 250], screen=screen)

        if cinematic_camera:
            screen.blit(pygame.image.load('images/cinematic_camera_on.png'), (screen.get_width() - 100, 100))
        pygame.display.flip()
        lustmoahos = moahos
        if moahos != pygame.mouse.get_pos():
            moahos = pygame.mouse.get_pos()
        # print(next_cwst_spown)
        if not next_cwst_spown:
            next_cwst_spown = random.randint(50, 500)
            for _ in range(random.randint(1, 80)):
                rabbits.append({'type': 'food', 'pos': (random.randint(-100000,
                                                                       100000),
                                                        random.randint(-100000, 10000)),
                                'food': 100, 'age': 0})

            # print(next_cwst_spown)
        next_cwst_spown -= 1
        fps.tick(60)
        # time.sleep(0.01)
        screen.fill(groun_color)
    pass


game()
